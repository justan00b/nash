# NaSH
A basic UNIX Shell written in C language and developed as a mini project.

## Dependencies
* GNU+Linux or other UNIX based OS
* C Compiler(gcc or clang)

## Usage
Run this program using terminal

```
$ git clone https://gitlab.com/justan00b/nash.git
$ cd nash
$ gcc shell.c -o nash
$ ./nash
```

## Author
[JUSTAN00B](https://gitlab.com/justan00b)
